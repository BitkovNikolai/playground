@wiremock
Feature: Wiremock tests feature

  Scenario: Default test
    Given I setup wiremock stub
    When I request wiremock "my/resource"
    Then I see that message is "SUCCESS"
    
  Scenario Outline: Request matching tests
    Given I setup wiremock stub
    When I request wiremock "<path>" with Content-Disposition "<test_type>"
    Then I see response is <response_code>
    Examples:
      | test_type | path       | response_code |
      | 1         | your/thing | 200           |
      | 2         | your/thing | 200           |
      | 3         | your/thing | 404           |

  Scenario Outline: Template tests
    Given I setup wiremock stub
    When I request wiremock "template" with template header "<header>"
    Then I see response is 200
    And I see response body contains "<header>"
    Examples:
      | header  |
      | temp    |
      | another |

  Scenario: Delay test
    Given I setup wiremock stub
    When I request wiremock "delay"
    Then I see response is 200
    And I check that the response time is more then 5000

  Scenario: Stateful test
    Given I setup wiremock stub
    When I send get to "stateful"
    Then I see response is 200
    And I see response body contains "current_state_Started"
    When I request wiremock "stateful"
    Then I see response is 201
    When I send get to "stateful"
    Then I see response is 200
    And I see response body contains "current_state_After"

  Scenario: Proxy test
    Given I setup wiremock stub
    When I send get to "ditto"
    Then I see response body contains "ditto"

  Scenario: Recorded stub test
    Given I setup wiremock stub
    When I send get to "pokemon/ditto"
    Then I see response body contains "ditto"