@pokemon
Feature: Default pokemon feature

  Scenario: Check that the weight of ditto is 40
    Given I request info about ditto pokemon
    When I get weight from the response
    Then I see that weight is 40

  Scenario Outline: Check that the ditto has move <move>
    Given I request info about ditto pokemon
    Then I check that move is <move>

    Examples:
    | move      |
    | transform |

  Scenario: Check the ditto abilities
    Given I request info about ditto pokemon
    Then I check that the abilities of pokemon is
      | limber   |
      | imposter |

  Scenario: Validate pokemon scheme
    Given I request info about ditto pokemon
    Then I check that the response with scheme pokemon

  Scenario: Check that there is a pikachu in pokemon list
    Given I request pokemon list
    Then There is a pikachu in pokemon list

  Scenario: Check that the content-type of result from pokemon breakpoint is application/json
    Given I request info about ditto pokemon
    Then header Content-Type is application/json

  Scenario: Response time is less then 2000 ms
    Given I request info about ditto pokemon
    Then I check that the response time is less then 2000