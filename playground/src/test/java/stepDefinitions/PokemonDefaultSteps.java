package stepDefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.junit.Assert;

public class PokemonDefaultSteps {

    private static final Logger logger = Logger.getLogger(PokemonDefaultSteps.class);
    private Response response;

    private Integer weight;

    @Before("@pokemon")
    public void setDefaultPathForPokemon() {
        RestAssured.baseURI = "https://pokeapi.co/api/v2/";
    }

    @Given("^I request info about (.*) pokemon$")
    public void i_request_url(String pokemon) {
        logger.info("getting info about "+pokemon);
        response = get("pokemon/{pokemon}",pokemon);
        response.then().statusCode(200);
    }

    @When("^I get weight from the response$")
    public void i_get_cookies_from_response() {
        weight = response.path("weight");
        logger.info("weight is " + weight);
    }

    @Then("I see that weight is {int}")
    public void iSeeThatWeightIs(Integer weight) {
        Assert.assertEquals(this.weight, weight);
    }

    @When("^I check that move is (.*)$")
    public void iCheckThatMoveIsMove(String move) {
        logger.info("Checking that the move is " + move);
        response.then().body("moves.move.name", contains(move));
    }

    @When("I check that the abilities of pokemon is")
    public void iCheckThatTheAbilitiesOfPokemonIs(List<String> abilities) {
        logger.info("Checking abilities");
        response.then().body("abilities.ability.name", is(abilities));
    }

    @When("^I check that the response with scheme (.*)$")
    public void iCheckThatTheResponseWithSchemePokemon(String scheme) {
        InputStream jsonScheme = getClass().getClassLoader()
            .getResourceAsStream("schemes/" + scheme + ".json");
        response.then().assertThat().body(matchesJsonSchema(jsonScheme));
    }

    @Given("I request pokemon list")
    public void iRequestPokemonList() {
        String path = "pokemon?limit=100000&offset=0";
        response = get(path);
        response.then().statusCode(200);
    }


    @Then("^There is a (.*) in pokemon list$")
    public void thereIsAPikachuInPokemonList(String pokemon) {
        ArrayList<String> pokemonNames = response.path("results.name");
        Assert.assertTrue(pokemonNames.contains(pokemon));
    }

    @Then("^header (.*) is (.*)$")
    public void headerContentTypeIsApplicationJson(String header, String value) {
        logger.info("Checking that header "+ header + " has value " + value);
        Assert.assertTrue((response.getHeader(header)).contains(value));
    }

    @Then("I check that the response time is less then {int}")
    public void iCheckThatTheResponseTimeIsLessThen(int responseTime) {
        logger.info("response time is " + response.getTime());
        response.then().time(lessThan((long) responseTime));
    }
}
