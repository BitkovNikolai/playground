package stepDefinitions;

import static io.restassured.RestAssured.given;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Attachment;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import outputCatcher.MultithreadedConsoleOutputCatcher;

public class AuthDefaultSteps {
    private static final Logger logger = Logger.getLogger(PokemonDefaultSteps.class);

    private Response response;

    @Before("@auth")
    public void setDefaultPathForPokemon() {
        RestAssured.baseURI = "https://authenticationtest.com/";
    }

    @Before
    public void beforeTest() {
        RestAssured.filters(new ResponseLoggingFilter(),new RequestLoggingFilter());
        MultithreadedConsoleOutputCatcher.startCatch();
    }
    @After
    public void afterTest() {
        stopCatch();
    }
    @SuppressWarnings("UnusedReturnValue")
    @Attachment(value = "Test Log", type = "text/plain")
    public String stopCatch() {
        String result = MultithreadedConsoleOutputCatcher.getContent();
        MultithreadedConsoleOutputCatcher.stopCatch();
        return result;
    }

    @When("I auth with simple auth")
    public void iAuthWithSimpleAuth() {
        response = given()
            .auth().basic("user","pass")
            .post("HTTPAuth/");
    }

    @Then("I see that auth is successful")
    public void iSeeThatAuthIsSuccessful() {
        response.then().assertThat().statusCode(302).assertThat().header("Location","https://authenticationtest.com/loginSuccess/");
    }
}
