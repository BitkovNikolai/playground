package stepDefinitions;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.lessThan;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.junit.Assert;

public class WiremockDefaultSteps {
    private static final Logger logger = Logger.getLogger(PokemonDefaultSteps.class);

    private Response response;


    private static boolean isWiremockServerStarted = false;
    @Before("@wiremock")
    public void setDefaultPathForPokemon() {
        RestAssured.baseURI = "http://localhost:8089/";
    }

    @Given("I setup wiremock stub")
    public void iSetupWiremockStub() {
        if (!isWiremockServerStarted) {
            WireMockServer wireMockServer = new WireMockServer(wireMockConfig()
                .port(8089)
                .extensions(new ResponseTemplateTransformer(true)));
            wireMockServer.start();
            isWiremockServerStarted = true;
        }
    }
    @When("I request wiremock {string}")
    public void iRequestWiremockStub(String path) {
        response = given()
            .header("Content-Type","xml")
            .post(path);
    }

    @Then("I see that message is {string}")
    public void iSeeThatMessageIs(String responseMessage) {
        response.then().assertThat().body("response", Matchers.equalTo(responseMessage));
    }

    @When("I request wiremock {string} with Content-Disposition {string}")
    public void iRequestWiremockRequestMatchingWith(String path, String contentDisposition) {
        response = given()
            .body("{ \"things\": [ { \"name\": \"RequiredThing\" }, { \"name\": \"Wiremock\" } ] }")
            .cookies("happy_cookie", "very happy cookie!",
                "regex_cookie", "anything")
            .header("Content-Disposition", contentDisposition)
            .get(path);
        }

    @Then("I see response is {int}")
    public void iResponseIs(int responseCode) {
        response.then().assertThat().statusCode(responseCode);
    }

    @When("I request wiremock {string} with template header {string}")
    public void iRequestWiremockWithTemplateHeader(String path, String header) {
        response = given()
            .header("template", header)
            .get(path);
    }

    @And("I see response body contains {string}")
    public void iSeeResponseBodyIs(String body) {
        Assert.assertTrue(response.asString().contains(body));
    }

    @And("I check that the response time is more then {int}")
    public void iCheckThatTheResponseTimeIsMoreThen(int responseTime) {
        response.then().time(Matchers.greaterThan((long) responseTime));
    }

    @When("I send get to {string}")
    public void iSendPostTo(String path) {
        response = given()
            .get(path);
    }
}
